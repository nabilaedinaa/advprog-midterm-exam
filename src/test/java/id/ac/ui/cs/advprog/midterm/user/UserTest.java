package id.ac.ui.cs.advprog.midterm.user;

import id.ac.ui.cs.advprog.midterm.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Autowired
    private static User user = new User();

    @Test
    void nameTest(){
        user.setName("Edina");
        String name = user.getName();
        assertEquals("Edina", user.getName());
        assertNotNull(name);
    }

    @Test
    void idTest(){
        user.setId(2626);
        long id = user.getId();
        assertEquals(2626, id);
    }

    @Test
    void emailTest(){
        user.setEmail("nabilaedinaaa@gmail.com");
        String email = user.getEmail();
        assertEquals("nabilaedinaaa@gmail.com", email);
        assertNotNull(email);
    }

    @Test
    void toStringMethodTest(){
        user.setId(2626);
        user.setName("Edina");
        user.setEmail("nabilaedinaaa@gmail.com");
        assertEquals("User{id=2626, name='Edina', email='nabilaedinaaa@gmail.com'}", user.toString());
    }

}
