package id.ac.ui.cs.advprog.midterm;

import id.ac.ui.cs.advprog.midterm.controller.UserController;
import id.ac.ui.cs.advprog.midterm.domain.User;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class MidtermProgrammingExamApplicationTests {

	@Autowired
	UserController userController;

	@Test
	void contextLoads() {
		MidtermProgrammingExamApplication.main(new String[]{});
		assertThat(userController).isNotNull();
	}

}
