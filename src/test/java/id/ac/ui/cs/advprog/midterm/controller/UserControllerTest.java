package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @MockBean
    private UserRepository userRepository;

    private User user = new User();

    public UserControllerTest() {
    }

    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(2626);
        user.setName("Edina");
        user.setEmail("nabilaedinaaa@gmail.com");
        when(userRepository.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    void signUpTemplateTest() throws Exception{
        mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    void addUserValidTest() throws Exception{
        user.setId(2626);
        user.setName("Edina");
        user.setEmail("nabilaedinaaa@gmail.com");
        this.mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));

    }

    @Test
    void addUserNotValidTest() throws Exception{
        user.setEmail(null);
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    void editUserTest() throws Exception{
        user.setId(2626);
        this.mockMvc.perform(get("/edit/2626"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("update-user"));

    }

    @Test
    public void editUserInvalidTest() throws Exception {
        try {
            this.mockMvc.perform(get("/edit/2626"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-user"))
                    .andExpect(model().attributeExists("user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:99", e.getMessage());
        }
    }

    @Test
    public void updateUserTest() throws Exception {
        user.setId(2626);
        user.setName("Edina");
        user.setEmail("nabilaedinaaa@gmail.com");
        this.mockMvc.perform(post("/update/" + user.getId())
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void updateUserInvalidTest() throws Exception {
        user.setEmail(null);
        this.mockMvc.perform(post("/update/" + 2626)
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void deleteUserTest() throws Exception {
        user.setId(2626);
        this.mockMvc.perform(get("/delete/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void deleteUserInvalidTest() throws Exception {
        try {
            this.mockMvc.perform(get("/delete/99"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:99", e.getMessage());
        }

    }












}
